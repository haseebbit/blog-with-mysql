## Prerequisites

* Python 2.7
* MySQL
* MySQL-Python
* SQLAlchemy

Test deployment
---------------

	$ git clone https://haseebbit@bitbucket.org/haseebbit/blog-with-mysql.git
	$ cd ~/blog-with-mysql
	$ pip install -r requirements.txt
	
## Edit flaskr.py

	mysql://username:password@server/db

Run the server
--------------
	$ python flaskr.py
